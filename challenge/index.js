const express = require('express');
const app = express();

const apiRouter = require('./src/routes/api');


app.use(express.json());
app.use(express.urlencoded({ extended:true }));

app.use('/api', apiRouter);

app.listen(3000, () => {
    console.log('Servidor en puerto 3000');
});

