const express = require('express');
const app = express();

require('./src/database/db');
require('./src/database/sync');


//const appiRouter = require('./src/routes/api');

app.use(express.json());
app.use(express.urlencoded({extended:true}));


//app.use('/')

app.listen(3000, () => {
    console.log('Servidor ejecutándose en puerto 3000');
});