const { Sequelize } = require('sequelize');
const { databaseAcamicadb } = require('./db');
const frutaModelo = require('../models/frutas');

const Frutas = frutaModelo(databaseAcamicadb, Sequelize);


databaseAcamicadb.sync({ force:false }).then(() => {
    console.log('tablas sincronizadas');
});

module.exports = {
    Frutas,
};