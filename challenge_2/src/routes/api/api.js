const apiFrutas = require('express').Router();

const router = require('../../../../../vigesimocuarta_sesion_mysqlii/programo_peliculas/routes/api');
const apiFrutas = require('./api/frutas');
const apiProductos = require('./api/productos/');
const apiusuarios = require('./api/usuarios/');

router.use('/frutas', apiFrutas);
router.use('./productos', apiProductos);
router.use('./usuarios', apiUsuarios);

module.exports = router;