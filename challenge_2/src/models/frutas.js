module.exports = (sequelize, type) => {
    return sequelize.define(
        "Frutas",
        {
            nombre: type.STRING,
            color: type.STRING,
            vitaminas: type.STRING,
        },
        {
            sequelize,
            timestamps: false, //para que no me cree el createdAt updatedAt
        }
    );
};